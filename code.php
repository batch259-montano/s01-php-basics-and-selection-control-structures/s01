<?php
// Activity 1

//In code.php, create a function named getFullAddress() that will take four arguments: 
// Country
// City
// Province
// Specific Address(such as block, lot, or building name and room number).


//Have this function return the concatenated arguments to result in a coherent address.

function getFullAddress($specificAddress,$city,$province,$country) {
    return "$specificAddress,$city,$province,$country";
}

//Activity 2

//Create a function named getLetterGrade() that uses conditional statements to output a letter representation of a given numerical grade:
// A+ (98 to 100)
// A (95 to 97)
// A- (92 to 94)
// B+ (89 to 91)
// B (86 to 88)
// B- (83 to 85)
// C+ (80 to 82)
// C (77 to 79)
// C- (75 to 76)
// F (75 below)

function getLetterGrade($letterGrade) {
    if($letterGrade > 98) {
        return 'is equivalent to A+';
    } else if ($letterGrade >= 95 && $letterGrade <= 97) {
        return 'is equivalent to A';
    } else if ($letterGrade >= 92 && $letterGrade <= 94) {
        return 'is equivalent to A-';
    } else if ($letterGrade >= 89 && $letterGrade <= 91) {
        return 'is equivalent to B+';
    } else if ($letterGrade >= 86 && $letterGrade <= 88) {
        return 'is equivalent to B';
    } else if ($letterGrade >= 83 && $letterGrade <= 85) {
        return 'is equivalent to B-';
    } else if ($letterGrade >= 80 && $letterGrade <= 82) {
        return 'is equivalent to C+';
    } else if ($letterGrade >= 77 && $letterGrade <= 79) {
        return 'is equivalent to C';
    } else if ($letterGrade >= 75 && $letterGrade <= 76) {
        return 'is equivalent to C-';
    } else {
        return 'Is equivalent to F';
    }
}