<?php require_once "./code.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S01 - PHP Basics and Selection Control Structures</title>
</head>
<body>
    <h1>Full Address</h1>
    <p> <?php echo getFullAddress('3F Caswyn Bldg',' Timog Avenue, Quezon City',' Metro Manila',' Philippines'); ?>
    <p> <?php echo getFullAddress('3F Enzo Bldg',' Buendia Avenue, Makati City',' Metro Manila',' Philippines'); ?>


    <h2>Letter-Based Grading</h2>
    <p>87 <?php echo getLetterGrade(87); ?> </p>
    <p>94 <?php echo getLetterGrade(94); ?> </p>
    <p>74 <?php echo getLetterGrade(74); ?> </p>

</body>
</html>